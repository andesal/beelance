from django.core import mail

class emailConnector:
    email_title_prefix = "[Beelance] "
    email_body_signature = ""
    fail_emails_silently = True
    sender_email = "Beelancer"

    @staticmethod
    def send_notification(title, body, recipients):
        with mail.get_connection(fail_silently=emailConnector.fail_emails_silently) as connection:
            mail.EmailMessage(
                emailConnector.email_title_prefix + title,
                body + emailConnector.email_body_signature, 
                emailConnector.sender_email,
                recipients,
                connection=connection,
            ).send()

