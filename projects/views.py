from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from .models import Project, Task, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, TaskFileTeam, directory_path
from .forms import ProjectForm, TaskFileForm, ProjectStatusForm, TaskOfferForm, TaskOfferResponseForm, TaskPermissionForm, DeliveryForm, TaskDeliveryResponseForm, TeamForm, TeamAddForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from user.models import Profile
from datetime import datetime, timezone
from django.contrib.sites.shortcuts import get_current_site
from django.contrib import messages
from django.utils import timezone
from beelance.notifications import emailConnector

def projects(request):
    return redirect('search')
    '''
    projects = Project.objects.all()
    project_categories = ProjectCategory.objects.all()
    return render(request,
        'projects/projects.html',
        {
            'projects': projects,
            'project_categories': project_categories,
        }
    )
    '''

@login_required
def new_project(request):
    current_site = get_current_site(request)
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.user = request.user.profile
            project.category =  get_object_or_404(ProjectCategory, id=request.POST.get('category_id'))
            project.location = request.POST.get('location')
            project.deadline = request.POST.get('deadline')
            project.save()

            people = Profile.objects.filter(categories__id=project.category.id)

            for person in people:
                if person.user.email:
                    emailConnector.send_notification(
                        "New Project: " + project.title,
                        "A new project you might be interested in was created and can be viewed at " + current_site.domain + '/projects/' + str(project.id),
                        [person.user.email]
                    )

            task_title = request.POST.getlist('task_title')
            task_description = request.POST.getlist('task_description')
            task_budget = request.POST.getlist('task_budget')
            for i in range(0, len(task_title)):
                Task.objects.create(
                    title = task_title[i],
                    description = task_description[i],
                    budget = task_budget[i],
                    project = project,
                )
            # Add sum of task budgets to project's total budget
            tasks = project.tasks.all()
            total_budget = 0
            for task in tasks:
                total_budget += task.budget
            project.total_budget = total_budget
            project.num_tasks = len(tasks)
            project.save()
            return redirect('project_view', project_id=project.id)
    else:
        form = ProjectForm()
    return render(request, 'projects/new_project.html', {'form': form})


def project_view(request, project_id):
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()

    if request.user == project.user.user:

        if request.method == 'POST' and 'offer_response' in request.POST:
            instance = get_object_or_404(TaskOffer, id=request.POST.get('taskofferid'))
            offer_response_form = TaskOfferResponseForm(request.POST, instance=instance)
            if offer_response_form.is_valid():
                offer_response = offer_response_form.save(commit=False)

                if offer_response.status == 'a':
                    offer_response.task.read.add(offer_response.offerer)
                    offer_response.task.write.add(offer_response.offerer)
                    project = offer_response.task.project
                    project.participants.add(offer_response.offerer)

                    # a notification is send to the freelancer in the case that the customer accepts his offer (and only then!!!)
                    emailConnector.send_notification(
                        "Offer accepted: "+ offer_response.title,
                        request.user.username + " has accepted your offer "+ offer_response.title+" for the following project: "+ project.title +".\n\nFeedback: "+ offer_response.feedback +"\n\nYou can start your work when project status is set to 'in Progress'.",
                        [offer_response.offerer.user.email]        
                    )
                   
                if offer_response.status == 'd':
                    # a notification is send to the freelancer in the case that the customer declines his offer (and only then!!!)
                    current_site = get_current_site(request)
                    emailConnector.send_notification(
                        "Offer declined: "+ offer_response.title,
                        request.user.username+" has declined your offer "+ offer_response.title +" for the following project: "+ project.title +".\n\nFeedback: "+ offer_response.feedback +"\n\nYou might adapt your offer at: "+ current_site.domain + '/projects/' + str(project.id)+".",
                        [offer_response.offerer.user.email]
                    )
                    
                offer_response.save()
        offer_response_form = TaskOfferResponseForm()

        if request.method == 'POST' and 'status_change' in request.POST:
            status_form = ProjectStatusForm(request.POST)
            if status_form.is_valid():
                project_status = status_form.save(commit=False)
                project.status = project_status.status
                if project.status == Project.INPROG:
                    project.date_started = datetime.now(timezone.utc)
                if project.status == Project.FINISHED:
                    project.date_finished = datetime.now(timezone.utc)
                project.save()

                #a noticication is send to all freelancer if the customer changes the status
                freelancers=project.participants.all()
                for person in freelancers:
                    if person.user.email:
                        emailConnector.send_notification(
                            "New project status: " + project.get_status_display(),
                            project.user.user.username+" has changed the status of project "+project.title+" to: "+project.get_status_display()+".",
                            [person.user.email]
                        )
                        
        status_form = ProjectStatusForm(initial={'status': project.status})

        return render(request, 'projects/project_view.html', {
        'project': project,
        'tasks': tasks,
        'status_form': status_form,
        'total_budget': project.cummulated_task_budget,
        'offer_response_form': offer_response_form,
        })


    else:
        if request.method == 'POST' and 'offer_submit' in request.POST:
            task_offer_form = TaskOfferForm(request.POST)
            if task_offer_form.is_valid():
                task_offer = task_offer_form.save(commit=False)
                task_offer.task =  Task.objects.get(pk=request.POST.get('taskvalue'))
                task_offer.offerer = request.user.profile
                task_offer.save()

                # a notification is sent to the customer when he receives an offer for a project task
                current_site = get_current_site(request)
                emailConnector.send_notification(
                    "New Offer: "+task_offer.title, 
                    " You have received an offer for your project: "+ project.title +".\n\nFreelancer: "+task_offer.offerer.user.username+" (First Name: "+task_offer.offerer.user.first_name+", Last Name: "+ task_offer.offerer.user.last_name+")\nTask title: "+task_offer.title+"\nTask description: "+task_offer.description+"\nTask price: "+str(task_offer.price)+"\n\nView this offer at: "+ current_site.domain + '/projects/' + str(project.id)+".",
                    [project.user.user.email]
                )

        task_offer_form = TaskOfferForm()

        return render(request, 'projects/project_view.html', {
        'project': project,
        'tasks': tasks,
        'task_offer_form': task_offer_form,
        'total_budget': project.cummulated_task_budget,
        })

@login_required
def upload_file_to_task(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    user_permissions = request.user.get_task_permissions(task)
    accepted_task_offer = task.accepted_task_offer()

    if user_permissions['modify'] or user_permissions['write'] or user_permissions['upload'] or request.user.is_project_owner(project):
        if request.method == 'POST':
            task_file_form = TaskFileForm(request.POST, request.FILES)
            if task_file_form.is_valid():
                task_file = task_file_form.save(commit=False)
                task_file.task = task
                existing_file = task.files.filter(file=directory_path(task_file, task_file.file.file)).first()
                access = user_permissions['modify'] or user_permissions['owner']
                for team in request.user.profile.teams.all():
                    file_modify_access  = TaskFileTeam.objects.filter(team=team, file=existing_file, modify=True).exists()
                    print(file_modify_access)
                    access = access or file_modify_access
                access = access or user_permissions['modify']
                if access:
                    if existing_file:
                        existing_file.delete()
                    task_file.save()

                    if request.user.profile != project.user and request.user.profile != accepted_task_offer.offerer:
                        teams = request.user.profile.teams.filter(task__id=task.id)
                        for team in teams:
                            tft = TaskFileTeam()
                            tft.team = team
                            tft.file = task_file
                            tft.read = True
                            tft.save()
                else:
                    messages.warning(request, "You do not have access to modify this file")

                return redirect('task_view', project_id=project_id, task_id=task_id)

        task_file_form = TaskFileForm()
        return render(
            request,
            'projects/upload_file_to_task.html',
            {
                'project': project,
                'task': task,
                'task_file_form': task_file_form,
            }
        )
    return redirect('/user/login')

@login_required
def task_view(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    accepted_task_offer = task.accepted_task_offer()

    user_permissions = user.get_task_permissions(task)
    if not user_permissions['read'] and not user_permissions['write'] and not user_permissions['modify'] and not user_permissions['owner'] and not user_permissions['view_task']:
        return redirect('/user/login')


    if request.method == 'POST' and 'delivery' in request.POST:
        if accepted_task_offer and accepted_task_offer.offerer == user.profile:
            deliver_form = DeliveryForm(request.POST, request.FILES)
            if deliver_form.is_valid():
                delivery = deliver_form.save(commit=False)
                delivery.task = task
                delivery.delivery_user = user.profile
                delivery.save()
                task.status = Task.PENDING_ACCEPTANCE
                task.save()

                # a notification is sent to the customer when he receives an delivery for a project task
                current_site = get_current_site(request)
                emailConnector.send_notification(
                    "New Delivery for: "+delivery.task.title,
                    "You have received a delivery for task "+delivery.task.title+" of your project "+project.title+" from "+accepted_task_offer.offerer.user.username+".\n\n Comment: "+delivery.comment+"\n\nView this delivery at: "+current_site.domain + '/projects/' + str(project.id)+ '/tasks/' +str(delivery.task.id)+".",
                    [project.user.user.email]                 
                )

    if request.method == 'POST' and 'delivery-response' in request.POST:
        instance = get_object_or_404(Delivery, id=request.POST.get('delivery-id'))
        deliver_response_form = TaskDeliveryResponseForm(request.POST, instance=instance)
        if deliver_response_form.is_valid():
            delivery = deliver_response_form.save()
            delivery.responding_time = timezone.now()
            delivery.responding_user = user.profile
            delivery.save()

            # a notification is sent to the freelancer when a delivery was accepted or declined
            if delivery.status == Delivery.ACCEPTED:
                task.status = Task.PENDING_PAYMENT
                task.save()
                emailConnector.send_notification(
                    "Delivery accepted for: "+ delivery.task.title,
                    project.user.user.username+" has accepted your delivery for the following task: "+ delivery.task.title +".\n\nFeedback: "+delivery.feedback+"\n\nYou should receive a payment soon.",
                    [accepted_task_offer.offerer.user.email]
                )
            elif delivery.status == Delivery.DECLINED:
                task.status = Task.DECLINED_DELIVERY
                task.save()
                
                current_site = get_current_site(request)
                emailConnector.send_notification(
                    "Delivery declined for: "+ delivery.task.title,
                    project.user.user.username+" has declined your delivery for the following task: "+ delivery.task.title +".\n\nFeedback: "+delivery.feedback+"\n\nYou might deliver a new file at: "+ current_site.domain + '/projects/' + str(project.id)+ '/tasks/' +str(delivery.task.id)+".",
                    [accepted_task_offer.offerer.user.email]
                )
                
    if request.method == 'POST' and 'team' in request.POST:
        if accepted_task_offer and accepted_task_offer.offerer == user.profile:
            team_form = TeamForm(request.POST)
            if team_form.is_valid():
                team = team_form.save(False)
                team.task = task
                team.save()


    if request.method == 'POST' and 'team-add' in request.POST:
        if accepted_task_offer and accepted_task_offer.offerer == user.profile:
            instance = get_object_or_404(Team, id=request.POST.get('team-id'))
            team_add_form = TeamAddForm(request.POST, instance=instance)
            if team_add_form.is_valid():
                team = team_add_form.save(False)
                team.members.add(*team_add_form.cleaned_data['members'])
                team.save()

    if request.method == 'POST' and 'permissions' in request.POST:
        if accepted_task_offer and accepted_task_offer.offerer == user.profile:
            for t in task.teams.all():
                for f in task.files.all():
                    try:
                        tft_string = 'permission-perobj-' + str(f.id) + '-' + str(t.id)
                        tft_id=request.POST.get(tft_string)
                        instance = TaskFileTeam.objects.get(id=tft_id)
                    except Exception as e:
                        instance = TaskFileTeam(
                            file = f,
                            team = t,
                        )

                    instance.read = request.POST.get('permission-read-' + str(f.id) + '-' + str(t.id))  or False
                    instance.write = request.POST.get('permission-write-' + str(f.id) + '-' + str(t.id)) or False
                    instance.modify = request.POST.get('permission-modify-' + str(f.id) + '-' + str(t.id))  or False
                    instance.save()
                t.write = request.POST.get('permission-upload-' + str(t.id)) or False
                t.save()


    deliver_form = DeliveryForm()
    deliver_response_form = TaskDeliveryResponseForm()
    team_form = TeamForm()
    team_add_form = TeamAddForm()


    if user_permissions['read'] or user_permissions['write'] or user_permissions['modify'] or user_permissions['owner'] or user_permissions['view_task']:
        deliveries = task.delivery.all()
        team_files = []
        teams = user.profile.teams.filter(task__id=task.id).all()
        per = {}
        for f in task.files.all():
            per[f.name()] = {}
            for p in f.teams.all():
                per[f.name()][p.team.name] = p
                if p.read:
                    team_files.append(p)
        return render(request, 'projects/task_view.html', {
                'task': task,
                'project': project,
                'user_permissions': user_permissions,
                'deliver_form': deliver_form,
                'deliveries': deliveries,
                'deliver_response_form': deliver_response_form,
                'team_form': team_form,
                'team_add_form': team_add_form,
                'team_files': team_files,
                'per': per
                })

    return redirect('/user/login')

@login_required
def task_permissions(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    accepted_task_offer = task.accepted_task_offer()
    if project.user == request.user.profile or user == accepted_task_offer.offerer.user:
        task = Task.objects.get(pk=task_id)
        if int(project_id) == task.project.id:
            if request.method == 'POST':
                task_permission_form = TaskPermissionForm(request.POST)
                if task_permission_form.is_valid():
                    try:
                        username = task_permission_form.cleaned_data['user']
                        user = User.objects.get(username=username)
                        permission_type =task_permission_form.cleaned_data['permission']
                        if permission_type == 'Read':
                            task.read.add(user.profile)
                        elif permission_type == 'Write':
                            task.write.add(user.profile)
                        elif permission_type == 'Modify':
                            task.modify.add(user.profile)
                    except Exception:
                        print("user not found")
                    return redirect('task_view', project_id=project_id, task_id=task_id)

            task_permission_form = TaskPermissionForm()
            return render(
                request,
                'projects/task_permissions.html',
                {
                    'project': project,
                    'task': task,
                    'form': task_permission_form,
                }
            )
    return redirect('task_view', project_id=project_id, task_id=task_id)


@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
