from django.test import TestCase, Client, tag
import projects.models as m
from projects.forms import TaskOfferForm
from user.models import User
from datetime import datetime, timezone
from django.core import mail
import sys


class BlackboxBoundaryTestCase(TestCase):
    #     #- Perform boundary value testing approaches in the web page to give project offer and sign-up page to detect bugs there.
    #     #- Perform output coverage test to identify bugs related to accepting offers.
    #     #- Perform 2-way domain-testing to test the sign-up page to identify bugs there.

    ## Give offer Boundaries
    # Im kinda just assuming that it shouldnt be possible to give negative values for offer price?
    # It currently does, but I feel like it shouldn't be allowed to do that?
    # Offer price: [0, ->
    # >>> Test for: < -1,0,1 >

    def setUp(self):
        self.user = User.objects.create(username='user')
        self.offerer = User.objects.create(username='offerer')
        self.category = m.ProjectCategory.objects.create(name="Test")
        self.project = m.Project.objects.create(category=self.category, user=self.user.profile)
        self.task = m.Task.objects.create(project=self.project)
        self.normal_form_data = {
            'title': 'Got a deal for you',
            'description': 'Something something not a scam',
            'price': 100
        }

    def test_give_offer_boundary_normal_price_value(self):
        # Test that a normal form works
        form_data = self.normal_form_data

        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())

    def test_give_offer_boundary_boundaryPlusone_price_value(self):
        # Test that boundary +1 works
        form_data = self.normal_form_data

        form_data['price'] = 1

        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())

    def test_give_offer_boundary_boundary_price_value(self):
        # Test that boundary works
        form_data = self.normal_form_data

        form_data['price'] = 0

        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())

    @tag('IGNORED_BUG')
    def test_give_offer_boundary_boundaryMinusOne_price_value(self):
        # Test that boundary -1 does NOT work
        form_data = self.normal_form_data

        form_data['price'] = -1

        form = TaskOfferForm(form_data)
        self.assertFalse(form.is_valid())

    def test_give_offer_max_price_value(self):
        # Test that there is no overflow work
        form_data = self.normal_form_data

        form_data['price'] = sys.maxsize

        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())

    @tag('IGNORED_BUG')
    def test_give_offer_boundary_min_price_value(self):
        # Test that boundary -1 does NOT work
        form_data = self.normal_form_data

        form_data['price'] = -sys.maxsize - 1

        form = TaskOfferForm(form_data)
        self.assertFalse(form.is_valid())


class ViewsTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='user', email='user@example.com')
        self.offerer = User.objects.create(username='offerer', email='offerer@example.com')
        self.anonymous = User.objects.create(username='anonymous')
        self.category = m.ProjectCategory.objects.create(name="Test")
        self.project = m.Project.objects.create(category=self.category, user=self.user.profile)
        self.task = m.Task.objects.create(project=self.project)
        self.offer = m.TaskOffer.objects.create(task=self.task, offerer=self.offerer.profile)
        self.team = m.Team.objects.create(task=self.task)

    def test_project_view(self):
        c = Client()
        response = c.get(f'/projects/{self.project.id}/')
        self.assertEqual(response.status_code, 200)

    def test_offer_response_accept(self):
        c = Client()
        c.force_login(self.user)

        response = c.post(f'/projects/{self.project.id}/', {
            'offer_response': 't',
            'feedback': 'Feedback',
            'status': 'a',
            'taskofferid': self.offer.id
        })

        self.offer.refresh_from_db()
        self.assertEqual(self.offer.status, 'a')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(response.status_code, 200)

    def test_offer_response_decline(self):
        c = Client()
        c.force_login(self.user)

        response = c.post(f'/projects/{self.project.id}/', {
            'offer_response': 't',
            'feedback': 'Feedback',
            'status': 'd',
            'taskofferid': self.offer.id
        })

        self.offer.refresh_from_db()
        self.assertEqual(self.offer.status, 'd')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(response.status_code, 200)

    def test_project_status_change(self):
        c = Client()
        c.force_login(self.user)

        response = c.post(f'/projects/{self.project.id}/', {
            'status_change': 't',
            'status': 'f'
        })

        self.project.refresh_from_db()
        self.assertEqual(self.project.status, 'f')
        self.assertEqual(response.status_code, 200)

    def test_project_status_change_inprog(self):
        c = Client()
        c.force_login(self.user)
        before_date = datetime.now(timezone.utc)

        response = c.post(f'/projects/{self.project.id}/', {
            'status_change': 't',
            'status': m.Project.INPROG
        })

        self.project.refresh_from_db()
        self.assertEqual(self.project.status, m.Project.INPROG)
        self.assertTrue(self.project.date_started > before_date)
        self.assertEqual(response.status_code, 200)

    def test_project_status_change_finished(self):
        c = Client()
        c.force_login(self.user)
        before_date = datetime.now(timezone.utc)

        self.project.date_started = before_date
        self.project.save()

        response = c.post(f'/projects/{self.project.id}/', {
            'status_change': 't',
            'status': m.Project.FINISHED
        })

        self.project.refresh_from_db()
        self.assertEqual(self.project.status, m.Project.FINISHED)
        self.assertTrue(self.project.date_finished > before_date)

        timedelta = self.project.date_finished - self.project.date_started
        self.assertEqual(self.project.duration, timedelta.total_seconds())

        self.assertEqual(response.status_code, 200)

    def test_project_status_change_email_freelancers(self):
        c = Client()
        c.force_login(self.user)
        before_date = datetime.now(timezone.utc)

        self.project.participants.add(self.offerer.profile)
        self.project.participants.add(self.user.profile)
        self.project.save()

        response = c.post(f'/projects/{self.project.id}/', {
            'status_change': 't',
            'status': m.Project.INPROG
        })

        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(response.status_code, 200)

    def test_offer_submit(self):
        c = Client()
        c.force_login(self.offerer)

        response = c.post(f'/projects/{self.project.id}/', {
            'offer_submit': 't',
            'taskvalue': self.task.id,
            'title': 'Test title',
            'description': 'Test description',
            'price': '200'
        })

        offer = m.TaskOffer.objects.last()
        self.assertEqual(offer.task.id, self.task.id)
        self.assertEqual(offer.title, 'Test title')
        self.assertEqual(response.status_code, 200)


from django.test import tag


class ProjectViewTestCase(TestCase):
    def setUp(self):
        pass

    @tag('IGNORED_BUG')
    def test_invalid_project_id(self):
        c = Client()
        # this might throw an exception
        response = c.get(f'/projects/100000/')

        self.assertNotEqual(response.status_code, 500, "The app should be able to handle an incorrect project id")
        self.assertEqual(response.status_code, 404,
                         "The app should respond with a 'NOT_FOUND' error when the project is not available")


class ProjectViewTestCaseSecurity(TestCase):

    def setUp(self):
        self.user1 = User.objects.create(username='user1')
        # self.user2 = User.objects.create(username='user2')

        self.offerer1 = User.objects.create(username='offerer1')
        self.offerer2 = User.objects.create(username='offerer2')

        self.category = m.ProjectCategory.objects.create(name="Test")

        self.project1 = m.Project.objects.create(category=self.category, user=self.user1.profile)
        self.task1 = m.Task.objects.create(project=self.project1)
        self.offer1 = m.TaskOffer.objects.create(task=self.task1, offerer=self.offerer1.profile)

        self.project2 = m.Project.objects.create(category=self.category, user=self.user1.profile)
        self.task2 = m.Task.objects.create(project=self.project2)
        self.offer2 = m.TaskOffer.objects.create(task=self.task2, offerer=self.offerer2.profile)

    @tag('IGNORED_BUG')
    def test_change_task_offer_via_different_project(self):
        """
        Test case:
            Try to change one task offer via the project page of another one.
            The aim is to show that the offer belonging to one project can be changed via the second project.

        """

        #

        c = Client()
        c.force_login(self.user1)

        data = {
            "status": "d",  # decline offer
            "feedback": "dont care",
            "taskofferid": self.offer2.id,  # !!! that shouldn't work -> wrong project
            "offer_response": ""
        }

        response = c.post('/projects/{}/'.format(self.project1.id), data)

        self.offer2.refresh_from_db()
        self.assertEqual(self.offer2.status, "a", "The offer status should be unchanged.")

    @tag('IGNORED_BUG')
    def test_post_anonymous_user(self):
        """
        Posting data as an anonymous user should be forbidden and therefore an E403 error returned.

        :return:
        """

        c = Client()

        data = {
            "status": "d",  # decline offer
            "feedback": "dont care",
            "taskofferid": self.offer1.id,
            "offer_response": ""
        }

        response = c.post('/projects/{}/'.format(self.project1.id), data)

        self.assertEqual(response.status_code, 401,
                         "The system should respond with 'UNAUTHORIZED' if posting as anonymous user")


class UploadFileToTaskTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='user')
        self.offerer = User.objects.create(username='offerer')
        self.category = m.ProjectCategory.objects.create(name="Test")
        self.project = m.Project.objects.create(category=self.category, user=self.user.profile)
        self.task = m.Task.objects.create(project=self.project)

    def test_upload_file_redirect(self):
        c = Client()
        c.force_login(self.offerer)
        response = c.post(f'/projects/{self.project.id}/tasks/{self.task.id}/upload/')
        self.assertEqual(response.status_code, 302)

    def test_post_file_as_owner(self):
        c = Client()
        c.force_login(self.user)
        c.post(f'/projects/{self.project.id}/tasks/{self.task.id}/upload/', data=self.__file_post_data())

    def test_post_file_as_offerer(self):
        c = Client()
        c.force_login(self.offerer)
        c.post(f'/projects/{self.project.id}/tasks/{self.task.id}/upload/', data=self.__file_post_data())

    @staticmethod
    def __file_post_data():
        import os
        import projects
        path = os.path.abspath(os.path.dirname(projects.__file__))
        data = {'file': open(os.path.join(path, "test_data", "report.txt"), "r")}
        return data
