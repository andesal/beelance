from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    company = models.TextField(max_length=50, blank=True)
    phone_number = models.TextField(max_length=50, blank=True)
    street_address = models.TextField(max_length=50, blank=True)
    city = models.TextField(max_length=50, blank=True)
    state = models.TextField(max_length=50, blank=True)
    postal_code = models.TextField(max_length=50, blank=True)
    country = models.TextField(max_length=50, blank=True)
    categories = models.ManyToManyField('projects.ProjectCategory', related_name='competance_categories')


    def __str__(self):
        return self.user.username

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

def get_task_permissions(self, task):
    if self.is_project_owner(task.project):
        return {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        }
    if task.accepted_task_offer() and task.accepted_task_offer().offerer == self.profile:
        return {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        }

    # Team members can view its teams tasks
    return {'write': self.profile.task_participants_write.filter(id=task.id).exists(),
            'read': self.profile.task_participants_read.filter(id=task.id).exists(),
            'modify': self.profile.task_participants_modify.filter(id=task.id).exists(),
            'owner': False,
            'view_task': self.profile.teams.filter(task__id=task.id).exists(),
            'upload': self.profile.teams.filter(task__id=task.id, write=True).exists()}

User.add_to_class('get_task_permissions', get_task_permissions)

def is_project_owner(self, project):
    return self == project.user.user

User.add_to_class('is_project_owner', is_project_owner)
