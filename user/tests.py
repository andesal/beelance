from django.test import TestCase, tag, Client
from user.models import User
from user.forms import SignUpForm
import projects.models as m

from django.http import HttpResponseRedirect
import sys
# Create your tests here.
class BlackboxSignupTestCase(TestCase):
    """

    Perform boundary value testing approaches in the web page to sign-up page to detect bugs there.

    """

    def setUp(self):
        self.user = User.objects.create(username='user')
        self.offerer = User.objects.create(username='offerer')
        self.anonymous = User.objects.create(username='anonymous')
        self.category = m.ProjectCategory.objects.create(name="Test")
        self.category = m.ProjectCategory.objects.create(name="Test2")
        self.normal_form_data = {
             'username':'t'*75,
             'first_name': 'Test',
             'last_name': 'Testmann',
             'categories': m.ProjectCategory.objects.all(),
             'company': 'TestaCo',
             'email': 'testman@test.co',
             'email_confirmation': 'testman@test.co',
             'password1': 'test0987'*4,
             'password2': 'test0987'*4,
             'phone_number': '123456789',
             'street_address': 'Test Avenue',
             'city': 'Testburg',
             'state': 'Testas',
             'postal_code': '7357',
             'country': 'Norway'
        }

    def test_signup_nonequal_password(self):
        form_data = self.normal_form_data

        form_data['password1'] = 'test09874'
        form_data['password2'] = 'test09873'

        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

    @tag('IGNORED_BUG')
    def test_signup_nonequal_email(self):
        form_data = self.normal_form_data

        form_data['email']              = 'testman@test.co'
        form_data['email_confirmation'] = 'tetsman@test.co'

        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid(), "Nonequal emails should not be accepted.")

    @tag('IGNORED_BUG')
    def test_signup_invalid_phone_number(self):
        form_data = self.normal_form_data

        form_data['phone_number'] = 'not_a_numerical_phone_number'

        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid(), "The invalid phone number should not be accepted.")

    def test_signup_normal_values(self):
        form_data = self.normal_form_data

        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def test_signup_name_max_boundaryPlusOne_value(self):
        form_data = self.normal_form_data

        form_data['username'] = 't'*151

        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

    def test_signup_name_max_boundaryMinusOne_value(self):
        form_data = self.normal_form_data

        form_data['username'] = 't'*149

        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def test_signup_name_max_boundary_value(self):
        form_data = self.normal_form_data

        form_data['username'] = 't'*150

        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def test_signup_name_mmin_boundaryPlusOne_value(self):
        form_data = self.normal_form_data

        form_data['username'] = 't'*2

        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def test_signup_name_min_boundaryMinusOne_value(self):
        form_data = self.normal_form_data

        form_data['username'] = 't'*0

        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

    def test_signup_name_min_boundary_value(self):
        form_data = self.normal_form_data

        form_data['username'] = 't'*1

        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def test_signup_password_boundaryMinusOne_value(self):
        form_data = self.normal_form_data

        form_data['password1'] = 'pjot098'
        form_data['password2'] = 'pjot098'

        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

    def test_signup_password_boundaryPlusOne_value(self):
        form_data = self.normal_form_data

        form_data['password1'] = 'test09870'
        form_data['password2'] = 'test09870'

        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def test_signup_password_boundary_value(self):
        form_data = self.normal_form_data

        form_data['password1'] = 'test0987'
        form_data['password2'] = 'test0987'

        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    @tag('IGNORED_BUG')
    def test_signup_non_numeric_postal_code(self):
        form_data = self.normal_form_data

        form_data['postal_code'] = 'non_numeric_postal_code'

        form = SignUpForm(form_data)
        # print(form.errors)
        self.assertFalse(form.is_valid(), "A non numeric postal code should not be accepted.")

    @tag('IGNORED_BUG')
    def test_signup_invalid_country(self):
        form_data = self.normal_form_data

        form_data['country'] = 'Utopia'

        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid(), "An unknown country should not be accepted.")

    @tag('IGNORED_BUG')
    def test_signup_invalid_first_name(self):
        form_data = self.normal_form_data

        form_data['first_name'] = '1234'

        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid(), "A numeric first name should not be accepted.")

    @tag('IGNORED_BUG')
    def test_signup_invalid_last_name(self):
        form_data = self.normal_form_data

        form_data['first_name'] = '5678'

        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid(), "A numeric last name should not be accepted.")


class SignupPersistenceTestCase(TestCase):
    """

    Checks if the signup form data is persisted properly

    """

    def setUp(self):
        self.anonymous = User.objects.create(username='anonymous')
        self.category = m.ProjectCategory.objects.create(name="Test")
        self.category = m.ProjectCategory.objects.create(name="Test2")
        self.normal_form_data = {
            'username': 't' * 2,
            'first_name': 'Test',
            'last_name': 'Testmann',
            'categories': m.ProjectCategory.objects.all(),
            'company': 'TestaCo',
            'email': 'testman@test.co',
            'email_confirmation': 'testman@test.co',
            'password1': 'test0987' * 4,
            'password2': 'test0987' * 4,
            'phone_number': '123456789',
            'street_address': 'Test Avenue',
            'city': 'Testburg',
            'state': 'Testas',
            'postal_code': '7357',
            'country': 'Testchia'
        }

    @tag('IGNORED_BUG')
    def test_signup_persistence(self):
        """
        Test to ensure that the form data is persisted into the database.

        Description: The user and its profile should be similar to the form data that was used to create the user

        """
        form_data = self.normal_form_data
        form = SignUpForm(form_data)

        # just to ensure that the form is saved
        self.assertTrue(form.is_valid())

        user = form.save()

        profile_fields = [
            "company", "phone_number", "street_address", "city", "state", "postal_code", "country"
        ]

        for profile_field in profile_fields:
            self.assertEqual(form_data[profile_field],
                             getattr(user.profile, profile_field),
                             "The profile field '%s' should be persisted" % profile_field)



class RoutingTestCase(TestCase):

    @tag("FIXED_BUG")
    def test_login_page_loading(self):
        c = Client()

        response = c.get(f'/user/login/')

        self.assertEqual(response.status_code, 200, "The login page should be loadable")

    @tag("FIXED_BUG")
    def test_sign_out_page(self):
        c = Client()

        user = User.objects.create(username='user')
        c.force_login(user)

        response = c.get(f'/user/logout/')

        self.assertEqual(response.status_code, 302,
                         "The user should be logged out when accessing that URI. "
                         "The user should then be redirected to the home page")

class ModelTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='user', email='user@example.com')
        self.offerer = User.objects.create(username='offerer', email='offerer@example.com')
        self.anonymous = User.objects.create(username='anonymous')
        self.category = m.ProjectCategory.objects.create(name="Test")
        self.project = m.Project.objects.create(category=self.category, user=self.user.profile)
        self.task = m.Task.objects.create(project=self.project)
        self.offer = m.TaskOffer.objects.create(task=self.task, offerer=self.offerer.profile)
        self.team = m.Team.objects.create(task=self.task)

    def test_get_task_permissions(self):
        res = self.user.get_task_permissions(self.task)
        self.assertEqual(res, {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True
        })

        self.offer.status = 'a'
        self.offer.save()

        res = self.offerer.get_task_permissions(self.task)
        self.assertEqual(res, {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True
        })

        res = self.anonymous.get_task_permissions(self.task)
        self.assertEqual(res, {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False
        })

        self.anonymous.profile.task_participants_write.add(self.task)
        res = self.anonymous.get_task_permissions(self.task)
        self.assertEqual(res, {
            'write': True,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False
        })
        self.anonymous.profile.task_participants_write.remove(self.task)

        self.anonymous.profile.teams.add(self.team)
        res = self.anonymous.get_task_permissions(self.task)
        self.assertEqual(res, {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': True,
            'upload': False
        })

        self.team.write = True
        self.team.save()
        res = self.anonymous.get_task_permissions(self.task)
        self.assertEqual(res, {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': True,
            'upload': True
        })

        self.anonymous.profile.task_participants_write.add(self.task)
        res = self.anonymous.get_task_permissions(self.task)
        self.assertEqual(res, {
            'write': True,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': True,
            'upload': True
        })

        self.anonymous.profile.task_participants_modify.add(self.task)
        res = self.anonymous.get_task_permissions(self.task)
        self.assertEqual(res, {
            'write': True,
            'read': False,
            'modify': True,
            'owner': False,
            'view_task': True,
            'upload': True
        })