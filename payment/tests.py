from django.test import TestCase, Client, tag
from user.models import User


class PaymentViewTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='user')

    @tag('IGNORED_BUG')
    def test_invalid_payment_arguments(self):
        c = Client()
        c.force_login(user=self.user)

        # this might throw an exception
        response = c.get(f'/payment/42/123')

        self.assertNotEqual(response.status_code, 500, "The app should be able to handle a incorrect arguments")
        self.assertEqual(response.status_code, 404,
                         "The app should respond with a 'NOT_FOUND' error when the project is not available")

    @tag('IGNORED_BUG')
    def test_invalid_receipt_arguments(self):
        c = Client()
        c.force_login(user=self.user)

        # this might throw an exception
        response = c.get(f'/payment/42/123/receipt/')

        self.assertNotEqual(response.status_code, 500, "The app should be able to handle incorrect arguments")
        self.assertEqual(response.status_code, 404,
                         "The app should respond with a 'NOT_FOUND' error when the project is not available")

