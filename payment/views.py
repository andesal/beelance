from django.shortcuts import render, redirect

from projects.models import Project, Task, TaskOffer
from projects.templatetags.project_extras import get_accepted_task_offer
from .forms import PaymentForm
from .models import Payment
from django.contrib.auth.decorators import login_required



@login_required
def payment(request, project_id, task_id):
    sender = Project.objects.get(pk=project_id).user
    task = Task.objects.get(pk=task_id)
    receiver = get_accepted_task_offer(task).offerer

    if request.method == 'POST':
        payment = Payment(payer=sender, receiver=receiver, task=task)
        payment.save()

        # a notification (mail) is sent to a freelancer when he is paid for a delivered task
        from django.core import mail
        try:
            with mail.get_connection() as connection:
                mail.EmailMessage("Payment received for: "+ payment.task.title , payment.payer.user.username +" has paid the open invoice for the following task: "+ payment.task.title + ".\n\nThank you very much for your support.","Beelancer",[receiver.user.email],connection=connection,).send()
        except Exception as e:
            from django.contrib import messages
            messages.error(request, 'Sending of email to '+ receiver.user.email +"failed: "+ str(e))

        task.status = Task.PAYMENT_SENT
        task.save()

        return redirect('receipt', project_id=project_id, task_id=task_id)

    form = PaymentForm()

    return render(request,
                'payment/payment.html', {
                'form': form,
                })

@login_required
def receipt(request, project_id, task_id):

    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    taskoffer = get_accepted_task_offer(task)

    return render(request,
                'payment/receipt.html', {
                'project': project,
                'task': task,
                'taskoffer': taskoffer,
                 })
