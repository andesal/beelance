var searchbar = document.getElementById("searchbar");
var searchRequest = null;
searchbar.onkeyup = function(e) {
    var that = this, value = $(this).val();
    if (searchRequest != null) 
        searchRequest.abort();
    searchRequest = $.ajax({
        type: "GET",
        url: "/search/",
        data: {
            'q' : value
        },
        dataType: "text",
        success: function(msg){
            //we need to check if the value is the same
            if (value==$(that).val()) {
                console.log(value)
            }
        }
    });
}