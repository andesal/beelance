var searchRequest = null;
var priceFrom = document.getElementById('price_from');
var priceTo = document.getElementById('price_to');
var searchBtn = document.getElementById('search_btn');
var searchForm = document.getElementById('search_form');
var searchBar = document.getElementById('searchbar'); 

priceFrom.onchange = function(){
    var x = priceFrom.value;
    var y = priceTo.value;
    if (y == null || x >= y) {
        priceTo.value = priceFrom.value;
    }
};

priceTo.onchange = function(){
    var x = priceFrom.value;
    var y = priceTo.value;
    if (x == null || y <= x) {
        priceFrom.value = priceTo.value;
    }
}

function clean(){
    var all_inputs = searchForm.getElementsByTagName('input');
    var input, i;
    for(i = 0; input = all_inputs[i]; i++) {
        if(input.getAttribute('name') && !input.value) {
            input.setAttribute('name', '');
        } 
    }

}
/*
$("#searchbar").keyup(function () {
    var that = this,
    value = $(this).val();
    if (searchRequest != null) 
        searchRequest.abort();
    searchRequest = $.ajax({
        type: "GET",
        url: "/search/dynamic_search/",
        data: {
            'q' : value, 
        },
        success: function(data){
            if (value==$(that).val()) {
                $('#search_results').html(data);
            }
        }
    });
});
*/
$(function() {
    var input = $("#searchbar");
    var len = input.val().length;
    if (input[0] != null) {
        input[0].focus();
        input[0].setSelectionRange(len, len);
    }
});

