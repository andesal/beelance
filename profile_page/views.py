from django.shortcuts import render, redirect
from user.models import Profile, User
from django.db.models import Avg
from projects.models import Project, ProjectCategory, Task
from django.db.models import Q
from search.views import filter_projects
from django.utils.safestring import mark_safe
from numbers import Number
from django.db.models import Avg

def profile_page(request, user_id):
    try:
        profile = Profile.objects.get(user_id=user_id)
    except Profile.DoesNotExist:
        return render(request, '404.html')
    
    ratings = User.objects.get(id=user_id).ratings
    overall_rating = ratings.aggregate(average_rating=Avg('stars'))['average_rating']
    projects = Project.objects.distinct().filter(ratings__in=ratings.all())
    tasks = Task.objects.filter(project__in=projects)
    results, search_url = filter_projects(request, tasks)
    search_url = "/profile/" + user_id + "?" + search_url
    stars_dict, desc_dict = {}, {}
    for rating in ratings.all():
        stars_dict[rating.project.pk] = stars(rating.project, user_id)
        desc_dict[rating.project.pk] = rating.description[0:50]

    # Get participants of user's projects
    user_projects = Project.objects.filter(user=user_id)
    freelancers = Profile.objects.distinct().filter(project_participants__in=user_projects)
 
    return render(request, 'profile_page/profile_view.html', {
        'overall_rating': overall_rating,
        'profile': profile,
        'ratings': ratings.all(), 
        'project_categories': ProjectCategory.objects.all(), 
        'results': results,
        'search_url': search_url,
        'stars_dict': stars_dict,
        'desc_dict': desc_dict,
        'freelancers': freelancers, 

    })

def stars(project, user_id):
    ratings = User.objects.get(id=user_id).ratings
    html_string = ''
    for r in ratings.all():
        if r.project == project:
            for _ in range(int(r.stars)):
                html_string += '<i class="fa fa-star" style="color:#CFB53B"></i>'

    return mark_safe('<span>%s</span>' % html_string)

