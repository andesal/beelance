#!/bin/bash

coverage run --source='.' manage.py test -v2 --exclude-tag IGNORED_BUG
coverage xml

pylint */**/*.py -r n --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" > pylint-report.txt


