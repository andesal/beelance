from django.test import TestCase, Client


class ViewsTestCase(TestCase):
    def setUp(self):
        pass

    def test_home_view(self):
        c = Client()
        response = c.get(f'')

        # if the user is not logged in, he should be redirected to the projects page
        self.assertEqual(response['location'], "/projects/")
        self.assertEqual(response.status_code, 302)
