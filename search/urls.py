from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.search, name='search'),
    #path('dynamic_search/', views.dynamic_search, name='dynamic_search'),
]
