from django.shortcuts import render
from projects.models import Project, ProjectCategory, Task
from django.db.models import Q
from django.shortcuts import redirect
from rating.templatetags.rating_extras import star_rating
from .utils import sort_projects_dict

def search(request):  
    results = filter_projects(request, None)
    
    search_url = "/search/?" + replace_search_url(request)

    return render(request, 'projects/search.html', {'results': results, 'project_categories': ProjectCategory.objects.all(), 'search_url': search_url})

def filter_projects(request, base_set):
    '''
        param: baseset: Queryset of tasks to filter. 
               If base_set=None all tasks of open projects are used for filtering
    '''
    tasks = []
    if base_set is not None:
      tasks = base_set
    else:
      tasks = Task.objects.filter(project__status=Project.OPEN)
    
    tasks = filter_by_keywords(request, tasks)
    tasks = filter_by_location(request, tasks)
    tasks = filter_by_category(request, tasks)
    
    projects = Project.objects.distinct().filter(tasks__in=tasks)
    projects = filter_by_price_range(request, projects)

    projects = sort_projects(request, projects)
    return projects

def replace_search_url(request):
    search_url = request.GET.urlencode()
    if '&sort=' in search_url:
      index = search_url.find("&sort=")
      search_url = search_url.replace(search_url[index:], "")
    if "sort=" in search_url: 
      index = search_url.find("sort=")
      search_url = search_url.replace(search_url[index:], "")
    return search_url

def filter_by_keywords(request, tasks):
    query = request.GET.get('q')  
    if query and query.strip() != "":
        query_list = query.split()
        for q in query_list:
            tasks = tasks.filter(Q(title__icontains=q) | Q(description__icontains=q) | Q(project__title__icontains=q) | Q(project__description__icontains=q) | Q(project__location__icontains=q) | Q(project__category__name__icontains=q))
    return tasks

def filter_by_location(request, tasks):
    search_location = request.GET.get('location')
    if search_location: 
        tasks = tasks.filter(project__location__iexact=search_location)
    return tasks

def filter_by_category(request, tasks):
    search_category = request.GET.get('category')
    if search_category: 
        tasks = tasks.filter(project__category__name__iexact=search_category)
    return tasks

def filter_by_price_range(request, projects):
    price_from = request.GET.get('from')
    price_to = request.GET.get('to')
    if price_from and price_to and price_from <= price_to:
        projects = projects.filter(Q(total_budget__gte=price_from) & Q(total_budget__lte=price_to))
    return projects
    
def sort_projects(request, projects):
    sort = request.GET.get('sort')
    if sort is not None:
        projects = projects.order_by(sort_projects_dict()[sort])
    return projects