
def sort_projects_dict(): 
    return {
      'price_asc': 'total_budget',
      'price_des': '-total_budget',
      'tasks_asc': 'num_tasks', 
      'tasks_des': '-num_tasks',
      'deadline': 'deadline',
      'finished':'date_finished', 
      'dur_asc': 'duration', 
      'dur_dec': '-duration', 
      'location': 'location', 
      'title': 'title'
    }