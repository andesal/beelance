from django.test import TestCase, Client

class SearchViewTestCase(TestCase):
    def setUp(self):
        return super().setUp()
    
    def test_status_code(self):
      c = Client()
      response = c.get(f'/search/?q=keyword&category=2&location=oslo&from=1&to=100')
      self.assertEqual(response.status_code, 200)