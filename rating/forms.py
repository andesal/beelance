from django import forms
from .models import Rating
from .utils import get_users_to_rate


class RatingForm(forms.ModelForm):
    stars = forms.IntegerField(min_value=0, max_value=5, initial=1)
    description = forms.CharField(min_length=100, max_length=500)

    def __init__(self, rater_user_id, project_id, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.rater_user_id = rater_user_id
        self.project_id = project_id

        # setting query set is not working here...
        self.user = forms.ModelChoiceField(queryset=get_users_to_rate(rater_user_id, project_id))

        self.field_order = ['user', 'stars', 'description']

        self.rater = forms.IntegerField(widget=forms.HiddenInput(), initial=rater_user_id)
        self.project = forms.IntegerField(widget=forms.HiddenInput(), initial=project_id)

        self.fields['project'].widget = forms.HiddenInput()
        self.fields['project'].initial = project_id

        self.fields['rater'].widget = forms.HiddenInput()
        self.fields['rater'].initial = rater_user_id

        self.fields['user'].queryset = get_users_to_rate(rater_user_id, project_id)

        # the form should only be displayed when a user can still rate another user
        self.rating_needed = self.fields['user'].queryset.count() > 0

    class Meta:
        model = Rating
        fields = ('user', 'stars', 'description', 'project', 'rater')

