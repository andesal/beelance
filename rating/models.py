from django.db import models
from user.models import User
from projects.models import Project
from django.core.validators import MinValueValidator, MaxValueValidator
from django.core.validators import MinLengthValidator, MaxLengthValidator


class Rating(models.Model):
    # the user that is rated
    user = models.ManyToManyField(User, related_name='ratings')
    # the rating is related to a project
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="ratings")
    # keep track who gave the rating
    rater = models.ForeignKey(User, on_delete=models.CASCADE, related_name="given_ratings")
    # FR1-4
    stars = models.IntegerField(validators=[MinValueValidator(limit_value=0), MaxValueValidator(limit_value=5)])
    # FR1-9
    description = models.TextField(max_length=500, validators=[MinLengthValidator(100), MaxLengthValidator(500)])
