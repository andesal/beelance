from django import template
from django.utils.safestring import mark_safe

from numbers import Number
from django.db.models import Avg

register = template.Library()

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.simple_tag
def star_rating(rating: (int, float)):
    """
    Create a html representation of the numerical star rating

    usage example:

        {% load rating_extras %}

        {% star_rating 5 %}

    :arg rating: the rating will most likely be float
    :return: html code
    """

    if rating is None:
        return mark_safe('')

    # assume list for now TODO fix
    if not isinstance(rating, Number):
        rating = rating.aggregate(average_rating=Avg('stars'))['average_rating']

        if rating is None: # if the above aggregation operates on an empty list, then rating is not valid
            return mark_safe('')

    html_string = ''
    for _ in range(int(rating)):
        html_string += '<i class="fa fa-star" style="color:#CFB53B"></i>'

    return mark_safe('<span>%s</span>' % html_string)


@register.filter
def project_needs_rating(project_id, user_id):
    """
    Filter to check whether project needs to be rated or is already complete.

    :param project_id: The PK of the project
    :param user_id:  The PK of the user
    :return: True if the project needs to be rated by the given user. False if rating is already complete.
    """
    from rating.utils import get_users_to_rate
    num_users = get_users_to_rate(user_id, project_id).count()
    return num_users > 0
