from .models import Project
from projects.models import TaskOffer
from user.models import User


def is_valid_task_participant(rater_user_id, project_id):
    project = Project.objects.get(pk=int(project_id))

    if project.user.id == rater_user_id:  # then the active user is a customer
        return True
    else:  # a task participant / project manager should only be able to rate the customer
        whitelist = TaskOffer.objects.filter(task_id__in=project.tasks.values_list('id')).filter(
            status=TaskOffer.ACCEPTED).values_list('offerer_id')

        return whitelist.intersection(User.objects.filter(pk=int(rater_user_id)).values_list('profile__id')).count() > 0


def get_users_to_rate(rater_user_id, project_id):
    project = Project.objects.get(pk=int(project_id))

    if project.user.id == rater_user_id:  # then the active user is a customer
        users_participated = project.participants.values_list('user')

        # remove all the participants that are not actively working on the project (e.g. only raised a task offer
        # but were not accepted)
        whitelist = TaskOffer.objects.filter(task_id__in=project.tasks.values_list('id')).filter(
            status=TaskOffer.ACCEPTED).values_list('offerer_id')

        users_participated = users_participated.intersection(whitelist)

    else:  # a task participant / project manager should only be able to rate the customer
        users_participated = User.objects.filter(pk=project.user.user.id).values_list('id')

    users_rated = project.ratings.values_list('user')
    users_to_rate = User.objects.filter(id__in=users_participated.difference(users_rated))

    return users_to_rate
