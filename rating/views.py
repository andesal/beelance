from django.http import HttpResponseRedirect

from django.views.generic import FormView
from django.utils.decorators import method_decorator
from django.shortcuts import render,get_object_or_404
from django.contrib.auth.decorators import login_required

from projects.models import Project
from .forms import RatingForm
from .utils import is_valid_task_participant


@method_decorator(login_required, name='dispatch')
class RatingView(FormView):
    template_name="rating/rating_view.html"
    form_class = RatingForm
    success_url = '/thanks/'

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        project_id = kwargs["project_id"]

        if not is_valid_task_participant(user_id, project_id):
            from django.contrib import messages
            messages.warning(request, "User is not allowed to view rating page for this project!")
            return HttpResponseRedirect('/')

        form = RatingForm(user_id, project_id)
        project = get_object_or_404(Project, pk=project_id)

        return render(request, self.template_name, {
            'project': project,
            'form': form,
            'rating_needed': form.rating_needed
        })

    def post(self, request, *args, **kwargs):
        project_id = kwargs["project_id"]
        user_id = request.user.id

        if not is_valid_task_participant(user_id, project_id):
            from django.contrib import messages
            messages.warning(request, "This user is not allowed to post a rating for this project!")
            return HttpResponseRedirect('/')

        form = RatingForm(data=request.POST, rater_user_id=user_id, project_id=project_id)

        if form.is_valid():
            rating = form.save(commit=True)
            rating.save()
            return HttpResponseRedirect('/rating/%d' % int(project_id))
        else:
            from django.contrib import messages
            messages.warning(request, "Invalid form!")

    def form_valid(self, form):
        return super(RatingView, self).form_valid(form)
