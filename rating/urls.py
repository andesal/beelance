from django.urls import path
from . import views

urlpatterns = [
    path('<project_id>/', views.RatingView.as_view(), name='rating_view'),
]
